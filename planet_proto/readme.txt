completed all exercises.
NOTE:
    1.  Need to understand how new() creates a separate namespace for
        each instance.
        In the earlier exercises, multiple objects that shared the same
        prototype could see each other's writes to the prototype object.
        But with new(), they couldn't unless the properties were added to
        the prototype object.
